* ### گزارش‌گیری

در این بخش متدهای  در دسترس شما قرار می‌گیرد.
همچنین در نظر داشته باشید که مسیر اصلی گزارش‌گیری از طریق پی پینگ به آدرس زیر می‌باشد.
```
{path}   https://IPG.PayPing.ir/v1/reports
```
* ### متد جزئیات تک پرداخت (PaymentDetails)
با استفاده از این متد شما می‌توانید گزارش جزئیات یک پرداخت را دریافت کنید. آدرس دریافت بصورت زیر تعریف می‌شود.
```
[GET] {path}/PaymentDetails?token={xxxx}
```
با ارسال کد یک پرداخت به آدرس فوق خروجی جزئیات به صورت زیر برگشت داده خواهد شد.
```
{
  "Amount": 1000,
  "PayDate": "1485173956",
  "Description": "تست پرداخت",
  "PaierName": "مسعود مشهدی",
  "PayerIdentity": "Email or PhoneNumbr",
  "Platform": "Windows 10",
  "Browser": "Firefox42",
  "Token": "xxxx",
  "RefId": "0002",
}
```

* ### متد پرداخت‌های تأیید شده (GetAllVerifiedPayments)
این متد کلیه پرداخت‌هایی را که تأییده پرداخت آنها گرفته شده است را به شما نشان خواهد داد.
```
[GET] {path}/GetAllVerifiedPayments
```
خروجی نیز بصورت زیر است.
```
[
  {
    "Amount": 100,
    "RefId": "100288038",
    "ReferenceId": "jhg",
    "PayDate": 1485173956
  },
  {
    "Amount": 100,
    "RefId": "100296695",
    "ReferenceId": "jhg",
    "PayDate": 1485364616
  }
  ....
]
```